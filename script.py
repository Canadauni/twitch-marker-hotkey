import requests as r
import json
import obspython as obs
import config
import os.path

# Hotkey code adapted from https://github.com/upgradeQ/OBS-Studio-Python-Scripting-Cheatsheet-obspython-Examples-of-API/blob/master/src/obs_httkeys.py
class Hotkey:
    def __init__(self, callback, obs_settings, _id):
        self.obs_data = obs_settings
        self.hotkey_id = obs.OBS_INVALID_HOTKEY_ID
        self.hotkey_saved_key = None
        self.callback = callback
        self._id = _id

        self.load_hotkey()
        self.register_hotkey()
        self.save_hotkey()

    def register_hotkey(self):
        description = "Htk " + str(self._id)
        self.hotkey_id = obs.obs_hotkey_register_frontend(
            "htk_id" + str(self._id), description, self.callback
        )
        obs.obs_hotkey_load(self.hotkey_id, self.hotkey_saved_key)
    
    def load_hotkey(self):
        self.hotkey_saved_key = obs.obs_data_get_array(
            self.obs_data, "htk_id" + str(self._id)
        )
        obs.obs_data_array_release(self.hotkey_saved_key)

    def save_hotkey(self):
        self.hotkey_saved_key = obs.obs_hotkey_save(self.hotkey_id)
        obs.obs_data_set_array(
            self.obs_data, "htk_id" + str(self._id), self.hotkey_saved_key
        )
        obs.obs_data_array_release(self.hotkey_saved_key)

class h:
    htk_copy = None

h1 = h()

def cb1(pressed):
    if pressed:
        with open(os.path.normpath('D:/Projects/hotkey_script/token.json'), 'r+') as j:
            token = json.load(j)
            refresh = token['refresh_token']
            new = r.post(f"https://id.twitch.tv/oauth2/token?grant_type=refresh_token&refresh_token={refresh}&client_id={config.client_id}&client_secret={config.client_secret}").json()
            access = new['access_token']
            j.seek(0)
            json.dump(new, j)
            j.truncate()
        username = 'blacsmit'
        headers = {
            "Authorization": f"Bearer {access}",
            "Client-id": config.client_id,
            "Content-Type": "application/json"
        }
        channel = r.get(f"https://api.twitch.tv/helix/users?login={username}", headers=headers).json()
        user_id = str(channel['data'][0]['id'])
        print(channel)
        marker = r.post(f"https://api.twitch.tv/helix/streams/markers", headers=headers, json={"user_id":user_id})
        print(marker.json())

def script_load(settings):
    h1.htk_copy = Hotkey(cb1, settings, "stream marker")

def script_save(settings):
    h1.htk_copy.save_hotkey()