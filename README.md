# OBS twitch stream marker

I wanted a way to place stream markers to twitch from OBS without having to focus a browser with the stream manager dashboard open. To run this script you need to have Python 3.6 installed some where on your PC and that location linked within the OBS scripts window.

Had to redo this by generating a access and refresh token. In the future there might be a way to integrate this in OBS but I'm doing it this way for now.

Basically you have to follow the steps outlined here: https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#oauth-authorization-code-flow

copy the token you generated into a `token.json` file

from there this code should work as long as the username is changed.